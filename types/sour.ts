import {
  DatePeriod,
  IChangeDateStructure,
  IGedcomCommonValuable,
  MultimediaLink,
  NoteStructure,
  Place,
  UserReferenceNumbers,
} from "./common";
import {
  AutomatedRecordId,
  ResponsibleAgency,
  SourceCallNumber,
  SourceDescriptiveTitle,
  SourceFileByEntry,
  SourceJurisdictionPlace,
  SourceMediaType,
  SourceOriginator,
  SourcePublicationFacts,
  TextFromSource,
  Xref,
} from "./primitives";

export interface ISourceRepositoryCitationStructure {
  Repositories?: ISourceRepositoryCitation[];
}
export interface ISourceRepositoryCitation extends IGedcomCommonValuable {
  Id: Xref;
  CallNumber?: SourceCallNumber | ICallNumber;
}
export interface ICallNumber extends IGedcomCommonValuable {
  Value: SourceCallNumber;
  Media?: SourceMediaType;
}

export interface IGedcomSource
  extends ISourceRepositoryCitationStructure,
    IChangeDateStructure,
    NoteStructure,
    MultimediaLink {
  Id: Xref;
  Data?: {
    Events?: IEventsRecorded[];
  };
  Agency?: ResponsibleAgency;
  Author?: SourceOriginator;
  Title?: SourceDescriptiveTitle;
  Abbrevation?: SourceFileByEntry;
  Publication?: SourcePublicationFacts;
  Text?: TextFromSource;
  ReferenceNumber?: UserReferenceNumbers;
  ReferenceIdNumber?: AutomatedRecordId;
}
export interface IEventsRecorded {
  Date?: DatePeriod;
  Place?: SourceJurisdictionPlace;
}
