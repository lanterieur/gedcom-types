import {
  IChangeDateStructure,
  AddressStructure,
  MultimediaLink,
  NoteStructure,
} from "./common";
import { AutomatedRecordId, SubmitterName, Xref } from "./primitives";

export interface IGedcomLLSubmitter
  extends AddressStructure,
    MultimediaLink,
    NoteStructure,
    IChangeDateStructure {
  Id: Xref;
  Name: SubmitterName;
  ReferenceIdNumber?: AutomatedRecordId;
}
