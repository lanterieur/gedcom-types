import {
  EventDetails,
  IGedCommonReference,
  IGedcomCommonValuable,
  IChangeDateStructure,
  INoteStructure,
  IMultimediaLink,
  SourceCitationStructure,
  NoteStructure,
  IIndividualAge,
} from "./common";
import {
  EventDescriptor,
  AutomatedRecordId,
  Xref,
  CountOfChildren,
  UserReferenceNumber,
} from "./primitives";

export interface IGedcomRelation
  extends IGedcomCommonValuable,
    IRelationEvents,
    IChangeDateStructure,
    INoteStructure,
    SourceCitationStructure,
    IMultimediaLink,
    IMarrAssociationStructure {
  // (FAM)
  Id: Xref;
  // HUSB
  Husband?: Xref;
  // WIFE
  Wife?: Xref;
  // CHIL
  Children?: Xref[];
  // NCHIL
  ChildrenCount?: CountOfChildren;
  // REFN
  Reference?: (UserReferenceNumber | IGedCommonReference)[];
  // RIN
  RecordIdNumber?: AutomatedRecordId;
}

type MarriageType =
  | "unknown"
  | "marriage"
  | "not married"
  | "civil"
  | "religious"
  | "common law"
  | "partnership"
  | "registered partnership"
  | "living together"
  | "living apart together";
export type RelationIsDescriptor =
  | "Witness_of_Marriage"
  | "Witness_of_Civil_Marriage"
  | "Witness_of_Religious_Marriage";
export interface IMarrAssociationStructure {
  Associates?: IMarrAssociate[];
}
export interface IMarrAssociate extends SourceCitationStructure, NoteStructure {
  Id: Xref;
  Relation: RelationIsDescriptor;
}
export interface IRelationEvents {
  Anulment?: IRelationEventDetails[];
  Census?: IRelationEventDetails[];
  Divorce?: IRelationEventDetails[];
  Engaged?: IRelationEventDetails[];
  MarriageBanns?: IRelationEventDetails[];
  MarriageContract?: IRelationEventDetails[];
  Marriage?: IMarriageEventDetails[];
  MarriageLicense?: IRelationEventDetails[];
  MarriageSettlement?: IRelationEventDetails[];
  Residence?: IRelationEventDetails[];
  Events?: (EventDescriptor | IRelationEventDetails)[];
}
export interface IRelationEventDetails extends EventDetails {
  Husband?: IIndividualAge;
  Wife?: IIndividualAge;
}
export interface IMarriageEventDetails extends IRelationEventDetails {
  Type?: MarriageType;
}
export interface IRelationEventEventDetails extends IRelationEventDetails {
  Name?: EventDescriptor;
}
