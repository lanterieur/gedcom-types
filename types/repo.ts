import {
  IChangeDateStructure,
  AddressStructure,
  NoteStructure,
  UserReferenceNumbers,
} from "./common";
import { AutomatedRecordId, Xref } from "./primitives";

export type NameOfRepository = string;

export interface IGedcomRepository
  extends AddressStructure,
    NoteStructure,
    IChangeDateStructure {
  Id: Xref;
  Name: NameOfRepository;
  ReferenceNumber: UserReferenceNumbers;
  ReferenceIdNumber: AutomatedRecordId;
}
