import {
  IChangeDateStructure,
  SourceCitationStructure,
  UserReferenceNumbers,
} from "./common";
import { AutomatedRecordId, UserText, Xref } from "./primitives";

export interface IGedcomNote
  extends SourceCitationStructure,
    IChangeDateStructure {
  Id: Xref;
  UserText: UserText;
  ReferenceNumber?: UserReferenceNumbers;
  ReferenceIdNumber?: AutomatedRecordId;
}
