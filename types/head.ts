import {
  IGedcomCommonValuable,
  AddressStructure,
  IFileCreationDate,
} from "./common";
import {
  FileCreationDate,
  CharacterEncoding,
  GedcomForm,
  VersionNumber,
  SystemId,
  LanguageOfText,
  Xref,
  NameOfBusiness,
  ReceivingSystemName,
  GedcomFileName,
  CopyrightGedcomFile,
  GedcomContentDescription,
  ProductVersionNumber,
  NameOfProduct,
  NameOfSourceData,
  PublicationDate,
  CopyrightSourceData,
} from "./primitives";

export interface IGedcomHead {
  // GEDC
  Gedcom: IGedcomHeadGedcom;
  // CHAR
  Characters: CharacterEncoding;
}
interface IGedcomHeadGedcom {
  // VERS
  Version: "5.5.5";
  // FORM
  Form: IGedcomForm;
}
interface IGedcomForm extends IGedcomCommonValuable {
  // (FORM)
  Value: GedcomForm;
  // VERS
  Version: VersionNumber;
}

export interface IGedcomLLHead extends IGedcomHead {
  // GEDC
  Gedcom: IGedcomLLHeadGedcom;
  // DEST
  Destination?: ReceivingSystemName;
  // SOUR
  Source: SystemId | IGedLLHeadSource;
  // DATE
  Date?: FileCreationDate | IFileCreationDate;
  // LANG
  Language?: LanguageOfText;
  // SUBM
  Submitter?: Xref;
  // FILE
  File?: GedcomFileName;
  // COPR
  Copyright?: CopyrightGedcomFile;
  // NOTE
  Note?: GedcomContentDescription;
}
interface IGedcomLLHeadGedcom extends IGedcomHeadGedcom {
  // FORM
  Form: IGedcomLLForm;
}
interface IGedcomLLForm extends IGedcomForm {
  // (FORM)
  Value: "LINEAGE-LINKED";
}
interface IGedLLHeadSource extends IGedcomCommonValuable {
  // (SOUR)
  Id: SystemId;
  // VERS
  Version?: ProductVersionNumber;
  // NAME
  Name?: NameOfProduct;
  // CORP
  Corporation?: NameOfBusiness | ICorporation;
  // DATA
  Data?: NameOfSourceData | IGedLLHeadSourceData;
}
interface ICorporation extends IGedcomCommonValuable, AddressStructure {
  // (DATA)
  Name: NameOfBusiness;
}
interface IGedLLHeadSourceData extends IGedcomCommonValuable {
  // (DATA)
  Name: NameOfSourceData;
  // DATE
  Date?: PublicationDate;
  // COPR
  Copyright?: CopyrightSourceData;
}
