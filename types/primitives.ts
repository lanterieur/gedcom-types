type DigitNoZero = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

// type TwoDigitsYear = `${Digit}${Digit}`;
type TwoDigitsYear = number;
type LessGreaterSymbolPrefix = "< " | "> " | "";
type NumberOfFullYears = `${number}y`;
type NumberOfFullMonths = `${number}m`;
type NumberOfFullDays = `${number}d`;
type AgeNumbers =
  | NumberOfFullDays
  | NumberOfFullMonths
  | NumberOfFullYears
  | `${NumberOfFullYears} ${NumberOfFullMonths} ${NumberOfFullDays}`
  | `${NumberOfFullYears} ${NumberOfFullMonths}`
  | `${NumberOfFullYears} ${NumberOfFullDays}`
  | `${NumberOfFullMonths} ${NumberOfFullDays}`;
// DEPRECATED
// type NamedAges = "CHILD" | "INFANT" | "STILLBORN";
export type AddressCity = string;
export type AddressCountry = string;
export type AddressEmail = string;
export type AddressFax = string;
export type AddressLine = string;
export type AddressPostalCode = string;
export type AddressState = string;
export type AddressWebAddress = string;
export type AdoptedByWhichParent = string;
/**
{Size=2:13}


[ NULL
| < + space
| > + space ]
]
[ YYY + y + space + MM + m + space + DDD + d
| YYY + y
| MM + m
| DDD + d
| YYY + y + space + MM + m
| YYY+ y + space + DDD + d
| MM + m + space + DDD + d
| CHILD
| INFANT
| STILLBORN
]
where:
> = greater than indicated age
< = less than indicated age
y = a label indicating years
m = a label indicating months
d = a label indicating days
YYY = number of full years, at most three digits
MM = number of months, at most 11, at most two digits
DDD = number of days, at most 365, at most three digits
CHILD = age < 8 years
INFANT = age < 1 year
STILLBORN = died just prior, at, or near birth, 0 years
space = U+0020, the Space character
 */
export type AgeAtEvent = `${LessGreaterSymbolPrefix}${
  | AgeNumbers
  | /* NamedAges| */ ""}`;
/**
{Size=1:90}
 */
export type AttributeDescriptor = string;
/*
{Size=4:4}
*/
type AttributeType =
  | "CAST"
  | "EDUC"
  | "NATI"
  | "OCCU"
  | "PROP"
  | "RELI"
  | "RESI"
  | "TITL"
  | "FACT";
/*
{Size=1:12}
*/
export type AutomatedRecordId = string | number;

/*
{Size=2:4}
*/
type BeforeCommonEra = "BCE" | "BC" | "B.C.";

/*
{Size=1:90}
*/
export type CasteName = string;
/*
{Size=1:90}
*/
export type CauseOfEvent = string;
/*
{Size=1:1}
0 = Unreliable evidence or estimated data
1 = Questionable reliability of evidence (interviews, census, oral genealogies, or potential
for bias for example, an autobiography)
2 = Secondary evidence, data officially recorded sometime after event
3 = Direct and primary evidence used, or by dominance of the evidence
*/
export type CertaintyAssessment = 1 | 2 | 3 | 0 | "1" | "2" | "3" | "0";
export type CharacterEncoding = "UTF-8" | "UNICODE";
/*
{Size=1:248}
*/
export type CopyrightGedcomFile = string;
/*
{Size=1:248}
A copyright statement required by the owner of data from which this information was
downloaded.
*/
export type CopyrightSourceData = CopyrightGedcomFile;
/*
{Size=1:3}

The known number of children of this individual from all relationships or, if subordinate to a
family group record, the reported number of children known to belong to this family group
(couple), regardless of whether the associated children are represented in the corresponding
structure. This i
*/
export type CountOfChildren = number;

/*
{Size=4:35}
When DateCalendarEscape is not present, "@#DGREGORIAN@" is assumed.
*/
export type Date = DateCalendar | `${DateCalendarEscape} ${DateCalendar}`;
/*
{Size=8:39}
*/
export type DateApproximated = `${DateApproximator} ${Date}`;
/*
ABT = About, meaning the date is not exact.
CAL = Calculated mathematically, for example, from an event date and age.
EST = Estimated based on an algorithm using some other event date.
*/
export type DateApproximator = "ABT" | "CAL" | "EST";
/*
{Size=4:15}
*/
export type DateCalendar =
  | DateGregorian
  | DateJulian
  | DateHebrew
  | DateFrenchR;
/*
{Size=4:15}
*/
export type DateCalendarEscape =
  | "@#DGREGORIAN@"
  | "@#DJULIAN@"
  | "@#DHEBREW@"
  | "@#DFRENCH R@"
  | "@#DUNKNOWN@" /* |"@#DSWEDISH@" */;
/*
{Size=10:11}
*/
export type DateExact = `${Day} ${Month} ${Year}`;
/*
{Size=4:35}
*/
export type DateFrenchR =
  | Year
  | `${MonthFrench} ${Year}`
  | `${Day} ${MonthFrench} ${Year}`;
/*
{Size=4:35}
*/
export type DateGregorian =
  | Year
  | `${Year} ${BeforeCommonEra}`
  | `${Month} ${Year}`
  | `${Day} ${Month} ${Year}`
  | `${Day} ${Month}`
  | `${Month} ${DualStyleYear}`
  | `${Day} ${Month} ${DualStyleYear}`;
/*
{Size=4:35}
*/
export type DateHebrew =
  | Year
  | `${MonthHebrew} ${Year}`
  | `${Day} ${MonthHebrew} ${Year}`;
/*
{Size=4:35}
*/
export type DateJulian =
  | Year
  | `${Year} ${BeforeCommonEra}`
  | `${Month} ${Year}`
  | `${Day} ${Month} ${Year}`
  | `${Month} ${DualStyleYear}`
  | `${Day} ${Month} ${DualStyleYear}`;
/*
{Size=7:35}

FROM = Indicates the beginning of a happening or state.
TO = Indicates the ending of a happening or state.
space = U+0020, the space character
Examples:
FROM 1904 TO 1915
= The state of some attribute existed from 1904 to 1915 inclusive.
FROM 1904
= The state of the attribute began in 1904 but the end date is unknown.
TO 1915
= The state ended in 1915 but the begin date is unknown.
*/
export type DatePeriod =
  | `FROM ${Date}`
  | `TO ${Date}`
  // | `FROM ${Date} TO ${Date}`; // Definition too complex
  | `FROM ${string} TO ${string}`;
/*
{Size=1:35}
A date phrase must be enclosed in matching parentheses.
*/
export type DatePhrase = Text;
/*
{Size=8:35}
AFT = Event happened after the given date.
BEF = Event happened before the given date.
BET = Event happened sometime between date 1 AND date 2
*/
export type DateRange =
  | `BEF ${Date}`
  | `AFT ${Date}`
  // | `BEF ${Date} AFT ${Date}`; // Definition too complex
  | `BEF ${string} AFT ${string}`;
/*
{Size=1:35}
*/
export type DateValue =
  | Date
  | DatePeriod
  | DateRange
  | DateApproximated
  | `(${DatePhrase})`
  | `INT ${Date} (${DatePhrase})`;
/*
{Size=1:2}

dd
Day of the month, where dd is a numeric value within the valid range of the days for the
associated calendar month.
This value must not start with a leading zero.
*/
// export type Day = `${DigitNoZero|""}${Digit}`;
export type Day = number;
/*
{Size=1:248}
*/
export type DescriptiveTitle = string;
/*
{Size=1:1}
*/
type Digit = DigitNoZero | 0;
/*
{Size=3:7}
*/
// export type DualStyleYear = `${Year}/${Digit}${Digit}`;
export type DualStyleYear = `${Year}/${TwoDigitsYear}`;

/*
{Size=1:90}
*/
export type EntryRecordingDate = DateValue;
/*
{Size=1:15}
*/
export type EventAttributeType =
  | EventTypeIndividual
  | EventTypeFamily
  | AttributeType;
/*
{Size=1:90}
*/
export type EventDescriptor = string;
/*
{Size=1:90}
*/
export type EventOrFactClassification = string;
/*
{Size=1:15
*/
export type EventTypeCitedFrom = EventAttributeType;
/*
{Size=3:4}
*/
export type EventTypeFamily =
  | "ANUL"
  | "CENS"
  | "DIV"
  | "DIVF"
  | "ENGA"
  | "MARR"
  | "MARB"
  | "MARC"
  | "MARL"
  | "MARS"
  | "EVEN";
/*
{Size=3:4}
*/
export type EventTypeIndividual =
  | "ADOP"
  | "BIRT"
  | "BAPM"
  | "BARM"
  | "BASM"
  | "BURI"
  | "CENS"
  | "CHR"
  | "CHRA"
  | "CONF"
  | "CREM"
  | "DEAT"
  | "EMIG"
  | "FCOM"
  | "GRAD"
  | "IMMI"
  | "NATU"
  | "RETI"
  | "PROB"
  | "WILL"
  | "EVEN";
/*
{Size=1:90}

An enumeration of the different kinds of events that were recorded in a particular source.
Each enumeration is separated by a comma. Such as a parish register of births, deaths, and
marriages would be BIRT, DEAT, MARR.
*/
// export type EventsRecorded = /EventAttributeType(?:,EventAttributeType)*/
export type EventsRecorded = string;

/*
{Size=10:11}
*/
export type FileCreationDate = DateExact;

/*
{Size=1:248}

A note that a user enters to describe the contents of the lineage-linked file in terms of
"ancestors or descendants of" so that the person receiving the data knows what genealogical
information the file contains
*/
export type GedcomContentDescription = string;
/*
{Size=5:248}
*/
export type GedcomFileName = string;
export type GedcomForm = string;

/*
{Size=1:30}
*/
export type IdNumber = string | number;
/*
{Size=1:15}
*/
export type LanguageId = LanguageIdLatin | LanguageIdUnicode;
export type LanguageIdLatin =
  | "Afrikaans"
  | "Albanian"
  | "Anglo-Saxon"
  | "Catalan"
  | "Catalan_Spn"
  | "Czech"
  | "Danish"
  | "Dutch"
  | "English"
  | "Esperanto"
  | "Estonian"
  | "Faroese"
  | "Finnish"
  | "French"
  | "German"
  | "Hawaiian"
  | "Hungarian"
  | "Icelandic"
  | "Indonesian"
  | "Italian"
  | "Latvian"
  | "Lithuanian"
  | "Navaho"
  | "Norwegian"
  | "Polish"
  | "Portuguese"
  | "Romanian"
  | "Serbo_Croa"
  | "Slovak"
  | "Slovene"
  | "Spanish"
  | "Swedish"
  | "Turkish"
  | "Wendic";
export type LanguageIdUnicode =
  | "Amharic"
  | "Arabic"
  | "Armenian"
  | "Assamese"
  | "Belorusian"
  | "Bengali"
  | "Braj"
  | "Bulgarian"
  | "Burmese"
  | "Cantonese"
  | "Church-Slavic"
  | "Dogri"
  | "Georgian"
  | "Greek"
  | "Gujarati"
  | "Hebrew"
  | "Hindi"
  | "Japanese"
  | "Kannada"
  | "Khmer"
  | "Konkani"
  | "Korean"
  | "Lahnda"
  | "Lao"
  | "Macedonian"
  | "Maithili"
  | "Malayalam"
  | "Mandarin"
  | "Manipuri"
  | "Marathi"
  | "Mewari"
  | "Nepali"
  | "Oriya"
  | "Pahari"
  | "Pali"
  | "Panjabi"
  | "Persian"
  | "Prakrit"
  | "Pusto"
  | "Rajasthani"
  | "Russian"
  | "Sanskrit"
  | "Serb"
  | "Tagalog"
  | "Tamil"
  | "Telugu"
  | "Thai"
  | "Tibetan"
  | "Ukrainian"
  | "Urdu"
  | "Vietnamese"
  | "Yiddish";
/*
{Size=1:15}
*/
export type LanguageOfText = LanguageId;
/*
{Size=3}

JAN = January
FEB = February
MAR = March
APR = April
MAY = May
JUN = June
JUL = July
AUG = August
SEP = September
OCT = October
NOV = November
DEC = December
*/
export type Month =
  | "JAN"
  | "FEB"
  | "MAR"
  | "APR"
  | "MAY"
  | "JUN"
  | "JUL"
  | "AUG"
  | "SEP"
  | "OCT"
  | "NOV"
  | "DEC";
/*
{Size=4}

VEND = VENDEMIAIRE
BRUM = BRUMAIRE
FRIM = FRIMAIRE
NIVO = NIVOSE
PLUV = PLUVIOSE
VENT = VENTOSE
GERM = GERMINAL
FLOR = FLOREAL
PRAI = PRAIRIAL
MESS = MESSIDOR
THER = THERMIDOR
FRUC = FRUCTIDOR
COMP = JOUR_COMPLEMENTAIRS
*/
export type MonthFrench =
  | "VEND"
  | "BRUM"
  | "FRIM"
  | "NIVO"
  | "PLUV"
  | "VENT"
  | "GERM"
  | "FLOR"
  | "PRAI"
  | "MESS"
  | "THER"
  | "FRUC"
  | "COMP";
/*
{Size=3}

TSH = Tishri
CSH = Cheshvan
KSL = Kislev
TVT = Tevet
SHV = Shevat
ADR = Adar
ADS = Adar Sheni
NSN = Nisan
IYR = Iyar
SVN = Sivan
TMZ = Tammuz
AAV = Av
ELL = Elul
*/
export type MonthHebrew =
  | "TSH"
  | "CSH"
  | "KSL"
  | "TVT"
  | "SHV"
  | "ADR"
  | "ADS"
  | "NSN"
  | "IYR"
  | "SVN"
  | "TMZ"
  | "AAV"
  | "ELL";
/*
{Size=1:259}
*/
export type MultimediaFileReference = SVGAnimatedString;
/*
{Size=3:4}

AAC = Advanced Audio Codec
AVI = Audio Video Interleaved (Windows)
BMP = BitMaP (Windows)
ePUB = Electronic Publication (ebook)
FLAC = Free Lossless Audio Codec
GIF = Graphics Interchange Format (CompuServe)
JPEG, JPG = Joint Photographic Experts Group
MKV = Matroska Video Container
mobi = MobiPocket (ebook)
MP3 = MPEG-2 Audio Layer III
PCX = Personal Computer eXchange (PaintBrush)
PDF = Portable Document Format
PNG = Portable Network Graphics
TIFF, TIF = Tagged Image File Format
WAV = WAVeform audio file format (Windows)
 */
export type MultimediaFormat =
  | "AAC"
  | "AVI"
  | "BMP"
  | "ePub"
  | "FLAC"
  | "GIF"
  | "JPEG"
  | "JPG"
  | "MKV"
  | "mobi"
  | "MP3"
  | "PCX"
  | "PDF"
  | "PNG"
  | "TIFF"
  | "TIF"
  | "WAV";

/*
{Size=1:90}

Name of the business, corporation, or person that produced or commissioned the product.
*/
export type NameOfBusiness = string;
/*
{Size=1:90}

The name of the software product that produced this GEDCOM file.
*/
export type NameOfProduct = string;
/*
{Size=1:90}

The official name of the archive in which the stated source material is stored.
*/
export type NameOfRepository = string;
/*
{Size=1:90}

The name of the electronic data source that was used to obtain the data in this GEDCOM
file. For example, the data may have been obtained from a CD-ROM disc that was named
“U.S. 1880 CENSUS CD-ROM vol. 13”.
*/
export type NameOfSourceData = string;
/*
{Size=1:120}
*/
export type NamePersonal =
  | NameText
  | `${NameText | ""}/${NameText}/${NameText | ""}`;
/*
{Size=1:120}
*/
export type NamePhonetic = NamePersonal;
/*
{Size=1:90}
*/
export type NamePiece = string;
/*
{Size=1:120}

Given name or earned
name. Different given names are separated by a comma.
*/
// export type NamePieceGiven = /NamePiece(?:, NamePiece)*/;
export type NamePieceGiven = string;
/*
{Size=1:30}

A descriptive or
familiar name used in connection with one's proper name.
*/
// export type NamePieceNickname = /NamePiece(?:, NamePiece)*/;
export type NamePieceNickname = string;
/*
{Size=1:30}

Non indexing name piece that appears preceding the given name and surname parts.
Different name prefix parts are separated by a comma.
*/
// export type NamePiecePrefix = /NamePiece(?:, NamePiece)*/;
export type NamePiecePrefix = string;
/*
{Size=1:120}

Surname or
family name. Different surnames are separated by a comma
*/
// export type NamePieceSurname = /NamePiece(?:, NamePiece)*/;
export type NamePieceSurname = string;
/*
{Size=1:30}

Surname prefix or article used in a family name.
A surname prefix that consists of multiple parts is written as is, and not modified in any way.
Thus, the surname prefix for the surname “de la Cruz” is “de la”.
*/
export type NamePieceSurnamePrefix = NamePiece;
/*
{Size=1:120}
*/
export type NameRomanised = NamePersonal;
/*
{Size=1:120}
*/
export type NameText = Text;
/*
{Size=5:30}
*/
export type NameType =
  | "aka"
  | "birth"
  | "immigrant"
  | "maiden"
  | "married"
  | string;
/*
{Size=1:120}

The person's division of national origin or other folk, house, kindred, lineage, or tribal
interest. Examples: Irish, Swede, Egyptian Coptic, Sioux Dakota Rosebud, Apache
Chiricawa, Navajo Bitter Water, Eastern Cherokee Taliwa Wolf, and so forth.
*/
export type NationalOrTribalOrigin = string;
/*
{Size=1:120}

The title given to or used by a person, especially of royalty or other noble class within a
locality.
*/
export type NobilityTypeTitle = string;
/*
{Size=1:3}

The number of different relationships (family groups) that this person was known to have
been a member of as a partner, regardless of whether the associated relationships are present
in the GEDCOM file.
*/
export type NumberOfRelationships = number;

/*
{Size=1:90}
The kind of activity that an individual does for a job, profession, or principal activity.
*/
export type Occupation = string;

/*
{Size=5:7}

adopted = indicates adoptive parents.
birth = indicates official parents (birth parents).
foster = indicates child was included in a foster or guardian family.
A code used to indicate the child to family relationship for pedigree navigation purposes.
When <PEDIGREE_LINKAGE_TYPE> is absent, official parentage (birth) is assumed.
*/
export type PedigreeLinkageType = "adopted" | "birth" | "foster";
export type PersonalName = `${string | ""}/${string}/${string | ""}`;
/*
{Size=1:25}
*/
export type PhoneNumber = string;
/*
{Size=5:30}

The phonetisation method used for creating the phonetic text.
<user defined> = record method used to arrive at the phonetic variation of the name.
Hangul = Phonetic method for transcribing Korean glyphs.
kana = Hiragana and/or Katakana characters were used in transcribing the
kanji character used by Japanese
*/
export type PhonetisationMethod = string | "hangul" | "kana";
/*
{Size=1:4095}

An unstructured list of the attributes that describe the physical characteristics of a person,
place, or object.
Commas separate each attribute.
Example:
1 DSCR Hair Brown, Eyes Brown, Height 5 ft 8 in
2 DATE 23 JUL 1935
*/
export type PhysicalDescription = string;
/*
{Size=2:10}

The value specifying the latitudinal coordinate of the place name. The latitude coordinate is
the direction North or South from the equator in degrees and fraction of degrees carried out
to give the desired accuracy. For example: 18 degrees, 9 minutes, and 3.4 seconds North
would be formatted as N18.150944. Minutes and seconds are converted by dividing the
minutes value by 60 and the seconds value by 3600 and adding the results together. This sum
becomes the fractional part of the degree’s value.
The shortest possible value is “N0”, the longest possible value, with 6 decimals precision is
“N89.123456”.
*/
export type PlaceLatitude = string;
/*
{Size=2:10}

The value specifying the longitudinal coordinate of the place name. The longitude
coordinate is Degrees and fraction of degrees east or west of the zero or base meridian
coordinate. For example: 168 degrees, 9 minutes, and 3.4 seconds East would be formatted
as E168.150944.
The shortest possible value is “E0”, the longest possible value, with 6 decimals precision is
“E179.123456”.
*/
export type PlaceLongidute = string;
/*
{Size=1:120}

The jurisdictional name of the place where the event took place. Jurisdictions are separated
by a comma and space combination. For example: "Cove, Cache, Utah, United States of
America".
No part of the place name may be replaced by an abbreviation. Place names are not
terminated by a full stop or anything else.
*/
// export type PlaceName = /PlaceText(?:, PlaceText)*/;
export type PlaceName = string;
/*
{Size=1:120}
*/
export type PlacePhonetic = string;
/*
{Size=1:120}
*/
export type PlaceRomanised = string;
/*
{Size=1:120}

A fully specified place name exists of several parts, from place name up to country,
separated by comma & space combinations, <PLACE_TEXT> is one such place name part.
As place name parts are separated by commas, including any commas within a place name
part would create confusion, so they have to be left out.
*/
export type PlaceText = Text;
/*
{Size=1:248}
*/
export type Possessions = string;
/*
{Size=3:15}

MMM + dot + mmm [ + dot + rrr [ + dot + bbb ] ]
where:
MMM = 1 through 3 digits; the major version number
mmm = 1 through 3 digits; the minor version number
rrr = 1 through 3 digits; the revision number
bbb = 1 through 3 digits; the build number
dot = U+002E, the Full Stop character
The version of the product that created the GEDCOM file.
The product version number is controlled by the developers of the product, but must be in
this format, and should co
*/
export type ProductVersionNumber = string;
/*
{Size=10:11}
*/
export type PublicationDate = DateExact;

/*
{Size=1:20}

The system identifier of the system expected to process the GEDCOM file.
*/
export type ReceivingSystemName = string;
/*
{Size=1:25}
*/
export type RelationIsDescriptor = string;
/*
{Size=1:90}
A name of the religion with which this person, event, or record was affiliated.
*/
export type ReligiousAffiliation = string;
/*
{Size=1:120}

The organization, institution, corporation, person, or other entity that has responsibility for
the associated context. For example, an employer of a person of an associated occupation, or
a church that administered rites or events, or an organization responsible for creating and/or
archiving records.
*/
export type ResponsibleAgency = string;
/*
{Size=1:25}

A word or phrase that identifies a person's role in an event being described. This should be
the same word or phrase, and in the same language, that the recorder used to define the role
in the actual record.
*/
export type RoleDescriptor = string;
/*
{Size=3:27}

Indicates what role this person played in the event that is being cited in this context. For
example, if you cite a child's birth record as the source of the mother's name, the value for
this field is "MOTH". If you describe the groom of a marriage, the role is "HUSB". If the
104
role is something different than one of the six relationship role tags listed above then enclose
the role name within matching parentheses.
The six predefined values must not be within parentheses, but that additional, systemdefined or user-defined values must be. It is an error to put any of the predefined values
within parentheses, or not use parentheses for other values.
*/
export type RoleInEvent =
  | "CHIL"
  | "HUSB"
  | "WIFE"
  | "MOTH"
  | "FATH"
  | "SPOU"
  | `(${RoleDescriptor})`;
/*
{Size=5:30}

Indicates the method used in transforming the text to a romanised transcription.
*/
export type RomanisationMethod = string | "pinyin" | "romaji" | "wadegiles";

/*
{Size=1:248}

A description of a scholastic or educational achievement or pursuit.
*/
export type ScholasticAchievement = string;
/*
{Size=1:1}

A code that indicates the sex of an individual:
M = Male (XY)
F = Female (XX)
X = Intersex (XXY)
U = Unknown (not found yet)
N = Not recorded
U is the default value.
The values U and N are mostly used for stillborn children, but must be used for every
situation where the sex is unknown (yet).
The difference between the two values is significant. The value U merely says that you do
not know the sex (yet), because you did not find or consult a record. The value N says that
the record did not record a sex, or that you are sure that there is no record.
*/
export type SexValue = "M" | "F" | "X" | "U" | "N";
/*
{Size=1:120}

An identification or reference description used to file and retrieve items from the holdings of
a repository.
*/
export type SourceCallNumber = string;
/*
{Size=1:4095}

The title of the work, record, or item and, when appropriate, the title of the larger work or
series of which it is a part.
For a published work, a book for example, might have a title plus the title of the series of
which the book is a part. A magazine article would have a title plus the title of the magazine
that published the article.
*/
export type SourceDescriptiveTitle = string;
/*
{Size=1:60}

This entry is to provide a short title used for sorting, filing, and retrieving source records.
*/
export type SourceFileByEntry = string;
/*
{Size=1:120}

The name of the lowest jurisdiction that encompasses all lower-level places named in this
source. For example, "Oneida, Idaho" would be used as a source jurisdiction place for events
occurring in the various towns within Oneida County. "Idaho" would be the source
jurisdiction place if the events recorded took place in other counties as well as Oneida
County
*/
export type SourceJurisdictionPlace = PlaceName;
/*
{Size=1:15}

A code, selected from one of the media classifications choices above, that indicates the type
of material in which the referenced source is stored.
*/
export type SourceMediaType =
  | "audio"
  | "book"
  | "card"
  | "electronic"
  | "fiche"
  | "film"
  | "magazine"
  | "manuscript"
  | "map"
  | "newspaper"
  | "photo"
  | "tombstone"
  | "video";
/*
{Size=1:255}

The person, agency, or entity who created the record. For a published work, this could be the
author, compiler, transcriber, abstractor, or editor. For an unpublished source, this may be an
individual, a government agency, church organization, or private organization, etc.
*/
export type SourceOriginator = string;
/*
{Size=1:4095}

When and where the record was created. For published works, this includes information
such as the city of publication, name of the publisher, and year of publication.
For an unpublished work, it includes the date the record was created and the place where it
was created. For example, the county and state of residence of a person making a declaration
for a pension or the city and state of residence of the writer of a letter.
*/
export type SourcePublicationFacts = string;
/*
{Size=1:60}

The name of the submitter formatted for display and address generation.
*/
export type SubmitterName = string;
/*
{Size=1:20}

A system identification name. This name must be unique for each system (product), different
from any other system. The name may include spaces, and is not restricted to ASCII
characters.
*/
export type SystemId = string;

/*
{Size=1:32767}
*/
export type Text = string;
/*
{Size=1:32767}

A verbatim copy of any description contained within the source. This indicates notes or text
that are actually contained in the source document, not the submitter's opinion about the
source. This should be, from the evidence point of view, "what the original record keeper
said" as opposed to the researcher's interpretation. The word TEXT, in this case, means from
the text which appeared in the source record including labels.
*/
export type TextFromSource = Text;
/*
{Size=7:12}

hh:mm[:ss[.fs]]
The time of a specific event, usually a computer-timed event, where:
hh = hours on a 24-hour clock, one or two digits; no leading zeroes {0:23}
mm = minutes, a two-digit value with leading zeroes {0:59}
ss = seconds, a two-digit value, with leading zeroes {0:59}
fs = a two-digit decimal fraction of a second {0:99}
*/
export type TimeValue =
  | `${number}:${number}`
  | `${number}:${number}:${number}`
  | `${number}:${number}:${number}.${number}`;

/*
{Size=1:20}

A user-defined number or text that the submitter uses to identify this record. For instance, it
may be a record number within the submitter's automated or manual system, or it may be a
page and position number on a pedigree chart
*/
export type UserReferenceNumber = number;
/*
{Size=1:40}

A user-defined definition of the <USER_REFERENCE_NUMBER>.
This value is free-form text, but meant to be a value from a relatively short user-defined list.
The <USER_REFERENCE_TYPE> is the line value of the optional REFN.TYPE record;
the REFN line value specifies a user reference number, the REFN.TYPE specifies that
number's type.
This allows users to classify the reference numbers they use in any way they see fit.
*/
export type UserReferenceType = string;
/*
{Size=1:32767}

Free-form user text. Comments, opinions.
*/
export type UserText = Text;

export type ValuePhonetic = string;
export type ValueRomanised = string;
export type VersionNumber = string;

/*
{Size=1:248}

Specific location within the information referenced. For a published work, this could include
the volume of a multi-volume work and the page number(s). For a periodical, it could
include volume, issue, and page numbers. For a newspaper, it could include a column
number and page number. For an unpublished source or microfilmed works, this could be a
film or sheet number, page number, frame number, etc. A census record might have an
enumerating district, page number, line number, dwelling number, and family number. The
data in this field should be in the form of a label and value pair, such as Label1: value,
Label2: value, with each pair being separated by a comma. For example, Film: 1234567,
Frame: 344, Line: 28.
*/
export type WhereWithinSource = string;
/*
{Size=3:22}

Either a pointer or an unique cross-reference identifier. If this element appears before the tag
in a GEDCOM line, then it is a cross-reference identifier. If it appears after the tag in a

GEDCOM line, then it is a pointer.
The at signs that delimit a cross-reference identifier or pointer are part of that cross-reference
identifier or pointer. The identifying part within the at signs has a minimum length of 1, and
maximum length of 20 code units.
*/
export type Xref = string;
/*
{Size=3:4}
*/
export type Year = number;
