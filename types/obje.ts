import {
  UserReferenceNumbers,
  NoteStructure,
  SourceCitationStructure,
  IChangeDateStructure,
} from "./common";
import {
  AutomatedRecordId,
  DescriptiveTitle,
  MultimediaFormat,
  SourceMediaType,
  Xref,
} from "./primitives";

export interface IGedcomMultimedia
  extends NoteStructure,
    SourceCitationStructure,
    IChangeDateStructure {
  Id: Xref;
  File: IMultimediaFileReference;
  ReferenceNumber?: UserReferenceNumbers;
  ReferenceIdNumber: AutomatedRecordId;
}
export interface IMultimediaFileReference {
  Format: MultimediaFormat | IMultimediaFormat;
  Title?: DescriptiveTitle;
}
export interface IMultimediaFormat {
  Name: MultimediaFormat;
  Type?: SourceMediaType;
}
