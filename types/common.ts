import {
  AddressCity,
  CopyrightGedcomFile,
  AddressCountry,
  AddressEmail,
  AddressFax,
  AddressLine,
  AddressPostalCode,
  AddressState,
  AddressWebAddress,
  AgeAtEvent,
  CauseOfEvent,
  CertaintyAssessment,
  DateExact,
  DateValue,
  EntryRecordingDate,
  EventOrFactClassification,
  EventTypeCitedFrom,
  PhonetisationMethod,
  PlaceLatitude,
  PlaceLongidute,
  PlaceName,
  RoleInEvent,
  RomanisationMethod,
  TextFromSource,
  TimeValue,
  UserReferenceNumber,
  UserReferenceType,
  ValuePhonetic,
  ValueRomanised,
  WhereWithinSource,
  Xref,
  FileCreationDate,
  ResponsibleAgency,
  RelationIsDescriptor,
  UserText,
  PhoneNumber,
} from "./primitives";

export interface NoteStructure {}
export interface SourceCitationStructure {
  Sources?: ISourceCitation;
}
export interface MultimediaLink {}


export interface IUserReferenceNumber {
  Id: UserReferenceNumber;
  Type?: UserReferenceType;
}
export type UserReferenceNumbers = (
  | UserReferenceNumber
  | IUserReferenceNumber
)[];

export interface IGedcomCommonValuable {
  Value?: string | number;
  Id?: string | number;
  Name?: string;
}

interface Address {
  Address1?: AddressLine;
  Address2?: AddressLine;
  Address3?: AddressLine;
  City?: AddressCity;
  State?: AddressState;
  PostalCode?: AddressPostalCode;
  Country?: AddressCountry;
}
export interface AddressStructure {
  Address: Address;
  Phone?: PhoneNumber;
  Email?: AddressEmail;
  Fax?: AddressFax;
  Web?: AddressWebAddress;
}
export interface Place extends AddressStructure {}
export interface Date extends IGedcomCommonValuable {
  Time?: TimeValue;
}
export interface IFileCreationDate extends IGedcomCommonValuable {
  Value: FileCreationDate;
  Copyright: CopyrightGedcomFile;
}
export interface DatePeriod extends Date {}

export interface IGedCommonReference extends IGedcomCommonValuable {
  // (REFN)
  Id: UserReferenceNumber;
  // TYPE
  Type: string;
}

export interface INoteStructure {
  Notes?: Xref | UserText;
}
export interface ISourceCitation
  extends IGedcomCommonValuable,
    MultimediaLink,
    NoteStructure {
  Id: Xref;
  Page?: WhereWithinSource;
  Events?: IEventTypeCitedFrom;
  Data?: ISourceData;
  QualityOfData?: CertaintyAssessment;
}
export interface ISourceData {
  Date?: EntryRecordingDate;
  Text?: TextFromSource[];
}
export interface IEventTypeCitedFrom extends IGedcomCommonValuable {
  Name: EventTypeCitedFrom;
  Role?: RoleInEvent;
}
export interface IMultimediaLink {
  Object?: Xref[];
}

export interface IChangeDate extends IGedcomCommonValuable, NoteStructure {
  Value: DateExact;
  Time?: TimeValue;
}
export interface IChangeDateStructure {
  Changed?: IChangeDate;
}

export interface IGedcomNotable {
  Notes?: string;
}
export interface IGedcomRef extends IGedcomNotable {
  Id: string;
}
export interface IGedcomIndividual extends IGedcomRef {
  Fullname?: string;
  Givenname?: string;
  Surname?: string;
  Nickname?: string;
  Sex?: string;
  Nationality?: string;
  Birth?: IGedcomEvent;
  Death?: IGedcomEvent;
  Burial?: IGedcomEvent;
  Adoption?: IGedcomEvent;
  Source?: IGedcomSourceRef;
  Relations?: (string | IGedcomRelationRef)[] | string | IGedcomRelationRef;
}
export interface IGedcomDate {
  Original?: string;
  HasYear?: boolean;
  HasMonth?: boolean;
  HasDay?: boolean;
  Value?: Date;
}
export interface IGedcomEvent extends IGedcomNotable {
  Name?: string;
  Date?: IGedcomDate;
  Place?: string;
  Source?: IGedcomSourceRef;
}
export interface IGedcomSourceRef extends IGedcomRef {
  Page?: string;
}
export interface IGedcomRelation extends IGedcomRef {
  Husband?: string;
  Wife?: string;
  Children?: string | string[] | IGedcomRef | IGedcomRef[];
  Marriage?: IGedcomEvent;
}
export interface IGedcomRelationRef {
  Value: string;
  Pedigree?: string;
}
export interface IGedcomRepoRef extends IGedcomRef {
  CallNumber?: string;
}
export interface IGedcomSource extends IGedcomRef {
  Title?: string;
  Name?: string;
  Data?: {
    Events?: IGedcomEvent;
    Abbreviation?: string;
    Agency?: string;
  };
  Repositories?: IGedcomRepoRef[] | IGedcomRepoRef;
}

export interface IGedcomRepository extends IGedcomRef {
  Name?: string;
  Address?: AddressStructure;
}

export interface IAssociationStructure {
  Associates?: IAssociate[];
}
export interface IAssociate extends SourceCitationStructure, NoteStructure {
  Id: Xref;
  Relation: RelationIsDescriptor;
}
export interface IMap {
  Latitude: PlaceLatitude;
  Longitude: PlaceLongidute;
}
export interface IPlace {
  Name: PlaceName;
  Phonetic?: (ValuePhonetic | IPhonetisation)[];
  Romanized?: (ValueRomanised | IRomanisation)[];
  Map?: IMap;
}
export interface IPlaceStructure extends IGedcomCommonValuable, NoteStructure {
  Places?: IPlace[];
}
export interface EventDetails
  extends IPlaceStructure,
    AddressStructure,
    NoteStructure,
    SourceCitationStructure,
    MultimediaLink {
  Type?: EventOrFactClassification;
  Date?: DateValue;
  Agency?: ResponsibleAgency;
  Cause?: CauseOfEvent;
}

export interface IIndividualAge {
  Age: AgeAtEvent;
}

export interface IPhonetisation extends IGedcomCommonValuable {
  Value: ValuePhonetic;
  Type: PhonetisationMethod;
}
export interface IRomanisation extends IGedcomCommonValuable {
  Type: RomanisationMethod;
}
