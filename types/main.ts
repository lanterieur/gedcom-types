import { IGedcomRepository } from "./repo";
import { IGedcomSource } from "./sour";
import { IGedcomRelation } from "./fam";
import { IGedcomHead, IGedcomLLHead } from "./head";
import { IGedcomIndividual } from "./indi";
import { IGedcomNote } from "./note";
import { IGedcomMultimedia } from "./obje";
import { IGedcomLLSubmitter } from "./subm";

export interface IGedcom555 {
  // HEAD
  Head: IGedcomHead;
}
export interface IGedcom555LineageLinked extends IGedcom555 {
  // HEAD
  Head: IGedcomLLHead;
  // SUBM
  Submitter: IGedcomLLSubmitter;
  // FAM
  Relations: IGedcomRelation[];
  // INDI
  Individuals: IGedcomIndividual[];
  // OBJE
  Object: IGedcomMultimedia[];
  // NOTE
  Notes: IGedcomNote[];
  // REPO
  Repositories: IGedcomRepository[];
  // SOUR
  Sources: IGedcomSource[];
}
