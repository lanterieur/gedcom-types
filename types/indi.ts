import {
  EventDetails,
  IAssociate,
  IChangeDateStructure,
  IGedcomCommonValuable,
  IIndividualAge,
  IMultimediaLink,
  INoteStructure,
  IPhonetisation,
  IRomanisation,
  NoteStructure,
  SourceCitationStructure,
  UserReferenceNumbers,
} from "./common";
import {
  AdoptedByWhichParent,
  AttributeDescriptor,
  AutomatedRecordId,
  CasteName,
  CountOfChildren,
  EventDescriptor,
  IdNumber,
  NamePieceGiven,
  NamePieceNickname,
  NamePiecePrefix,
  NamePieceSurname,
  NamePieceSurnamePrefix,
  NameType,
  NationalOrTribalOrigin,
  NobilityTypeTitle,
  NumberOfRelationships,
  Occupation,
  PedigreeLinkageType,
  PersonalName,
  PhysicalDescription,
  Possessions,
  ReligiousAffiliation,
  ScholasticAchievement,
  SexValue,
  UserReferenceType,
  ValuePhonetic,
  ValueRomanised,
  Xref,
} from "./primitives";

export interface IPersonalNamePieces
  extends NoteStructure,
    SourceCitationStructure {
  NamePrefix?: NamePiecePrefix;
  GivenName?: NamePieceGiven;
  NickName?: NamePieceNickname;
  SurnamePrefix?: NamePieceSurnamePrefix;
  Surname?: NamePieceSurname;
  Rufnamen?: string;
}
export interface IPersonalNameStructure {
  FullName?: PersonalName | IPersonalName;
}
export interface IPersonalName
  extends IPersonalNamePieces,
    IGedcomCommonValuable {
  Value: PersonalName;
  Type?: NameType;
  Phonetic?: (ValuePhonetic | IPhoneticName)[];
  Romanized?: (ValueRomanised | IRomanisedName)[];
}

export interface IPhoneticName extends IPhonetisation, IPersonalNamePieces {}
export interface IRomanisedName extends IRomanisation, IPersonalNamePieces {}
export interface IIndividualEventAsChildDetail extends IIndividualEventDetail {
  Relations?: Xref;
}
export interface IIndividualEventAdoptDetail extends IIndividualEventDetail {
  Relations?:
    | Xref
    | {
        Id: Xref;
        Adoption: AdoptedByWhichParent;
      };
}
export interface IIndividualEventEvent extends IIndividualEventDetail {
  Name?: EventDescriptor;
}
export interface IIndividualEventStructure {
  Birth?: IIndividualEventAsChildDetail;
  Christening?: IIndividualEventAsChildDetail | true;
  Death?: IIndividualEventDetail | true;
  Burial?: IIndividualEventDetail;
  Cremation?: IIndividualEventDetail;
  Adoption?: IIndividualEventAdoptDetail[];
  Baptism?: IIndividualEventDetail;
  BasMitzvah?: IIndividualEventDetail;
  AdultChristening?: IIndividualEventDetail;
  FirstCommunion?: IIndividualEventDetail;
  Confirmation?: IIndividualEventDetail;
  Naturalization?: IIndividualEventDetail[];
  Emigration?: IIndividualEventDetail[];
  Immigration?: IIndividualEventDetail[];
  Census?: IIndividualEventDetail[];
  Probate?: IIndividualEventDetail[];
  Will?: IIndividualEventDetail[];
  Graduation?: IIndividualEventDetail[];
  Retirement?: IIndividualEventDetail[];
  Events?: (EventDescriptor | IIndividualEventEvent)[];
}
export interface IIndividualEventDetail
  extends IGedcomCommonValuable,
    EventDetails,
    IIndividualAge {}
export interface IIndividualAttribute extends IIndividualEventDetail {
  Value: string | number;
  Type?: UserReferenceType;
}
export interface IIndividualAttributeCaste extends IIndividualAttribute {
  Value: CasteName;
}
export interface IIndividualAttributePhysicalDescription
  extends IIndividualAttribute {
  Value: PhysicalDescription;
}
export interface IIndividualAttributeScholasticAchievement
  extends IIndividualAttribute {
  Value: ScholasticAchievement;
}
export interface IIndividualAttributeIdNumber
  extends IIndividualAttributeTypeRequired {
  Value: IdNumber;
}
export interface IIndividualAttributeNationalOrTribalOrigin
  extends IIndividualAttribute {
  Value: NationalOrTribalOrigin;
}
export interface IIndividualAttributeCountOfChildren
  extends IIndividualAttribute {
  Value: CountOfChildren;
}
export interface IIndividualAttributeNumberOfRelationships
  extends IIndividualAttribute {
  Value: NumberOfRelationships;
}
export interface IIndividualAttributeOccupation extends IIndividualAttribute {
  Value: Occupation;
}
export interface IIndividualAttributePossessions extends IIndividualAttribute {
  Value: Possessions;
}
export interface IIndividualAttributeReligiousAffiliation
  extends IIndividualAttribute {
  Value: ReligiousAffiliation;
}
export interface IIndividualAttributeNobilityTypeTitle
  extends IIndividualAttribute {
  Value: NobilityTypeTitle;
}
export interface IIndividualAttributeFact
  extends IIndividualAttributeTypeRequired {
  Value: AttributeDescriptor;
}
export interface IIndividualAttributeTypeRequired extends IIndividualAttribute {
  Type: UserReferenceType;
}

export interface IIndividualAttributeStructure {
  Caste?: IIndividualAttributeCaste;
  PhysicalDescription?: IIndividualAttributePhysicalDescription;
  Education?: IIndividualAttributeScholasticAchievement;
  IdentNumber?: IIndividualAttributeIdNumber;
  Nationality?: IIndividualAttributeNationalOrTribalOrigin;
  ChildrenCount?: IIndividualAttributeCountOfChildren;
  MarriageCount?: IIndividualAttributeNumberOfRelationships;
  Occupation?: IIndividualAttributeOccupation;
  Property?: IIndividualAttributePossessions;
  Religion?: IIndividualAttributeReligiousAffiliation;
  Residence?: IIndividualAttribute;
  Title?: IIndividualAttributeNobilityTypeTitle;
  Fact?: IIndividualAttributeFact;
}
export interface IChildToFamilyLink
  extends IGedcomCommonValuable,
    NoteStructure {
  Id: Xref;
  Pedigree?: PedigreeLinkageType;
}
export interface IChildToFamilyLinkStructure {
  Relations?: (Xref | IChildToFamilyLink)[];
}
export interface ISpouseToFamilyLinkStructure {
  Relations?: (Xref | ISpouseToFamilyLink)[];
}
export interface ISpouseToFamilyLink
  extends IGedcomCommonValuable,
    NoteStructure {
  Id: Xref;
}
export type RelationIsDescriptor =
  | "Godparent"
  | "Witness_Of_Birth"
  | "Witness_Of_Death";
export interface IIndiAssociationStructure {
  Associates?: IIndiAssociate[];
}
export interface IIndiAssociate extends IAssociate {
  Id: Xref;
  Relation: RelationIsDescriptor;
}

export interface IGedcomIndividual
  extends IPersonalNameStructure,
    IIndividualEventStructure,
    IIndividualAttributeStructure,
    IChildToFamilyLinkStructure,
    ISpouseToFamilyLinkStructure,
    IIndiAssociationStructure,
    IChangeDateStructure,
    INoteStructure,
    SourceCitationStructure,
    IMultimediaLink {
  Id: Xref;
  Sex: SexValue;
  ReferenceNumber: UserReferenceNumbers;
  RecordIdNumber: AutomatedRecordId;
  Relations?: (Xref | ISpouseToFamilyLink | IChildToFamilyLink)[];
}
